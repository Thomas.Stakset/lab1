package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        
            while (true) {
            String computerChoice = rpsChoices.get((int)(Math.random() * 3));
            System.out.println("Let's play round " + roundCounter);
            roundCounter ++;
            
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();

            if(computerChoice.equals(humanChoice)) {
                System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Its a tie!");
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);  
            }   
                else if (humanChoice.equals("rock") && computerChoice.equals("scissors")) {
                System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Human wins!");
                humanScore ++;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore); 
            }
                else if (humanChoice.equals("paper") && computerChoice.equals("rock")) {
                System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Human wins!");
                humanScore ++;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
                else if (humanChoice.equals("scissors") && computerChoice.equals("paper")){
                System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Human wins!");
                humanScore ++;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
                
                else if (humanChoice.equals("rock") && computerChoice.equals("paper")) {
                    System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Computer wins!");
                    computerScore ++;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore); 
            }
                    else if (humanChoice.equals("paper") && computerChoice.equals("scissors")) {
                    System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Computer wins!");
                    computerScore ++;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
                    else if (humanChoice.equals("scissors") && computerChoice.equals("rock")){
                    System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Computer wins!");
                    computerScore ++;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
                    else {
                    System.out.println("I do not understand " + humanChoice + ". Could you please try again?");
                    }
            

                String playAgain = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
                if(playAgain.equals("y")) {
                continue;
            }
                else if (playAgain.equals("n")) {
                    System.out.println("Bye bye :)");
                break;
            }
        }
           
        
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
